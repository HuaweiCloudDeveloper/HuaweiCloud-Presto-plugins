### 背景

Presto是Facebook开源的，完全基于内存内存的并⾏计算，分布式SQL交互式查询引擎，采用Massively parallel processing (MPP)架构，多个节点管道式执⾏，⽀持任意数据源（通过扩展式Connector组件），数据规模GB~PB级。

### 项目简介

本项目将基于Presto的扩展式connector机制，完成对华为云OBS，DataArts，DWS的支持，包含但不限于提供功能文档，使用文档，代码贡献到Presto&Trino仓库等等

### 参考资料

- [OBS](https://support.huaweicloud.com/bestpractice-obs/obs_05_1507.html)
- [Presto Connector](https://prestodb.io/docs/current/connector.html)

待添加...

